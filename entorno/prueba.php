<?php
    require_once('conexion.php');
    class Accion{
        public function __construct() {$this->conn = new Conexion();}    
        
        public function consultar(){
            $sentenciaSql = "SELECT * FROM `gimnasioinfantil`.`ubicacion.pais`";
            $this->conn->preparar($sentenciaSql);
            $this->conn->ejecutar();
            return true;
        }
        public function __destruct() {        
            unset($this->conn); 
        }        
    }

    $accion='CONSULTAR';
    switch($accion){
        case 'CONSULTAR':
            try{
                $accion = new Accion();
                $resultado = $accion->consultar();                
                $numeroRegistros = $accion->conn->obtenerNumeroRegistros();
                if($numeroRegistros === 1){
                    if ($rowBuscar = $accion->conn->obtenerObjeto()){
                        echo $rowBuscar->codigo;                        
                    }
                }
            }catch(Exception $e){
                ?><script language="javascript">alert("Error, no fué posible consultar la información, consulte con el administrador.");</script><?php                
            }
        break;
    }
    
?>