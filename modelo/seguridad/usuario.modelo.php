<?php

require_once '../../entorno/conexion.entorno.php';
require '../../controlador/seguridad/usuario.controlador.php';

if(isset($_POST['accion']))
{

    ?><script language="javascript">alert("<?php echo $_POST['accion'] ?>");</script><?php


    switch($_POST['accion'])
    {
        case 'ADICIONAR':
            try {
                $usuario = new Usuario();
                $usuario->setCodigo($_POST['txtCodigo']);
                $usuario->setDescripcion($_POST['txtDescripcion']);
                $usuario->setClave($_POST['txtClave']);
                $usuario->setEstado($_POST['cmbEstado']);
                $usuario->agregar();

                ?><script language="javascript">alert("La información se adicionó correctamente.");</script><?php
            } catch(Exception $ex) {
                ?><script language="javascript">alert("Error, no fué posible adicionar la información, consulte con el administrador.");</script><?php
            }
        break;
        case 'MODIFICAR':
            try {
                $usuario = new Usuario();
                $usuario->setDescripcion($_POST['txtDescripcion']);
                $usuario->setClave($_POST['txtClave']);
                $usuario->setEstado($_POST['cmbEstado']);
                $usuario->modificar();

                ?><script language="javascript">alert("La información se adicionó correctamente.");</script><?php
            } catch(Exception $ex) {
                ?><script language="javascript">alert("Error, no fué posible adicionar la información, consulte con el administrador.");</script><?php
            }
        break;
        case 'ELIMINAR':
            try {
                $usuario = new Usuario();
                $usuario->setEstado($_POST['cmbEstado']);
                $usuario->setIdUsuario($_POST['hidIdUsuario'])
                $usuario->eliminar();

                ?><script language="javascript">alert("La información se adicionó correctamente.");</script><?php
            } catch(Exception $ex) {
                ?><script language="javascript">alert("Error, no fué posible adicionar la información, consulte con el administrador.");</script><?php
            }
        break;
        case 'CONSULTAR':
            try{
                $usario = new Usuario();
                $usario->setIdUsuario($_POST['hidIdUsuario']);
                $usario->setCodigo($_POST['txtCodigo']);
                $usario->setDescripcion($_POST['txtDescripcion']);                
                $usario->consultar();
                $numeroRegistros = $usario->conn->obtenerNumeroRegistros();
                if($numeroRegistros === 1){
                    if ($rowBuscar = $usario->conn->obtenerObjeto()){
                        $_POST['hidIdUsuario'] = $rowBuscar->idUsuario;
                        $_POST['txtDescripcion'] = $rowBuscar->descripcion;
                        $_POST['txtCodigo'] = $rowBuscar->codigo;
                        $_POST['txtClave'] = $rowBuscar->clave;
                        $_POST['cmbEstado'] = $rowBuscar->estado;
                    }
                }
            }catch(Exception $e){
                ?><script language="javascript">alert("Error, no fué posible consultar la información, consulte con el administrador.");</script><?php
            }
            break;
        case 'CONSULTAR_INICIO_SESION':
            try{
                $usuario = new Usuario();
                $usuario->setCodigo($_POST['txtCodigo']);
                $usuario->setClave(md5($_POST['txtClave']));
                $usuario->consultar();
                $numeroRegistros = $usuario->conn->obtenerNumeroRegistros();
                if($numeroRegistros === 1)
                {
                    if($rowBuscar = $usuario->conn->obtenerObjeto())
                    {
                        $_SESSION['id_Usuario'] = $rowBuscar->idUsuario;
                        $_SESSION['descripcion'] = $rowBuscar->descripcion;
                    }
                }
                else
                {
                    ?><script language="javascript">alert("Error, usuario no existe.");</script><?php
                }
            } catch(Exception $e){
                ?><script language="javascript">alert("Error, usuario no existe.");</script><?php
            }
        break;
    }

}


?>