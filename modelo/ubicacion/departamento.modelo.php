<?php
//$ubicacionFormulario =  substr($_SERVER["SCRIPT_NAME"], 17);
//require '/../seguridad/permiso.log.php';
require_once '../../entorno/conexion.entorno.php';
require '../../controlador/ubicacion/departamento.controlador.php';
//require '../../logica/seguridad/botones.log.php';

if(isset($_POST['accion']))
{
    switch ($_POST['accion']){
        case 'ADICIONAR':
            try{
                $ubicacion = new Departamento();
                $ubicacion->setIdPais($_POST['cmbPais']);
                $ubicacion->setCodigo($_POST['txtCodigo']);
                $ubicacion->setDescripcion($_POST['txtDescripcion']);                
                $ubicacion->agregar();

                ?><script language="javascript">alert("La información se adicionó correctamente.");</script><?php
            }catch(Exception $e){
                ?><script language="javascript">alert("Error, no fué posible adicionar la información, consulte con el administrador.");</script><?php
            }
        break;
        case 'MODIFICAR':
            try{
                $ubicacion = new Departamento();
                $ubicacion->setIdPais($_POST['hidIdPais']);
                $ubicacion->setIdPais($_POST['cmbPais']);
                $ubicacion->setCodigo($_POST['txtCodigo']);
                $ubicacion->setDescripcion($_POST['txtDescripcion']);
                $ubicacion->setEstado($_POST['cmbEstado']);
                $ubicacion->modificar();

                ?><script language="javascript">alert("La información se modificó correctamente.")</script><?php
            }catch(Exception $e){
                ?><script language="javascript">alert("Error, no fué posible modificar la información, consulte con el administrador.");</script><?php
            }
        break;
        case 'ELIMINAR':
            try{
                $ubicacion = new Departamento();
                $ubicacion->setIdDepartamento($_POST['hidIdDepartamento']);
                $ubicacion->eliminar();

                ?><script language="javascript">alert("La información se eliminó correctamente.")</script><?php            
            }catch(Exception $e){
                ?><script language="javascript">alert("Error, no fué posible eliminar la información, consulte con el administrador.");</script><?php
            }
        break;
        case 'CONSULTAR':
            try{
                $ubicacion = new Departamento();
                $ubicacion->setIdDepartamento($_POST['hidIdDepartamento']);
                $ubicacion->setCodigo($_POST['txtCodigo']);
                $ubicacion->setDescripcion($_POST['txtDescripcion']);                
                $ubicacion->consultar();
                $numeroRegistros = $ubicacion->conn->obtenerNumeroRegistros();
                if($numeroRegistros === 1){
                    if ($rowBuscar = $ubicacion->conn->obtenerObjeto()){
                        $_POST['hidIdDepartamento'] = $rowBuscar->idDepartamento;
                        $_POST['txtDescripcion'] = $rowBuscar->descripcion;
                        $_POST['txtCodigo'] = $rowBuscar->codigo;
                        $_POST['cmbEstado'] = $rowBuscar->estado;
                    }
                }
            }catch(Exception $e){
                 ?><script language="javascript">alert("Error, no fué posible consultar la información, consulte con el administrador.");</script><?php
            }
        break;
    }
}
?>
