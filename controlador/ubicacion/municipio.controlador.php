<?php
class Municipio{
    
    private $idMunicipio;
    private $idDepartamento;
    private $codigo;
    private $descripcion;
    private $estado;
    private $fechaCreacion;
    private $fechaModificacion;
    private $idUsuarioCreacion;
    private $idUsuarioModificacion;
    public  $conn=null;
    
    //idMunicipio
    public function getIdMunicipio(){return $this->idMunicipio;}
    public function setIdMunicipio($idMunicipio){$this->idMunicipio = $idMunicipio;}

    //idDepartamento
    public function getIdDepartamento(){return $this->idDepartamento;}
    public function setIdDepartamento($idDepartamento){$this->idDepartamento = $idDepartamento;}
    
    //codigo
    public function getCodigo(){return $this->codigo;}
    public function setCodigo($codigo){$this->codigo = $codigo;}

    //descripcion
    public function getDescripcion(){return $this->descripcion;}
    public function setDescripcion($descripcion){$this->descripcion = $descripcion;}
    
    //estado
    public function getEstado(){return $this->estado;}
    public function setEstado($estado){$this->estado = $estado;}
    
    //fechaCreacion
    public function getFechaCreacion(){ return $this->fechaCreacion;}
    public function setFechaCreacion($fechaCreacion) { $this->fechaCreacion =$fechaCreacion;}
    
    //fechaModificacion
    public function getFechaModificacion(){ return $this->fechaModificacion;}
    public function setFechaModificacion($fechaModificacion) { $this->fechaModificacion =$fechaModificacion;}
    
    //idUsuarioCreacion
    public function getIdUsuarioCreacion(){ return $this->idUsuarioCreacion;}
    public function setIdUsuarioCreacion($id_usuario) { $this->idUsuarioCreacion =$id_usuario;}
    
    //idUsuarioModificacion
    public function getIdUsuarioModificacion(){ return $this->idUsuarioModificacion;}
    public function setIdUsuarioModificacion($id_usuario) { $this->idUsuarioModificacion =$id_usuario;}
    
    //contructor
    public function __construct() {$this->conn = new Conexion();}
    
    public function agregar(){
        $sentenciaSql = "
                            , @estado = '$this->estado'
                            , @id_usuario_creacion = $_SESSION[id_usuario]
                            , @id_usuario_modificacion = $_SESSION[id_usuario]
                        ";
        $this->conn->preparar($sentenciaSql);
        $this->conn->ejecutar();
        return true;
    }
    
    public function modificar(){
        $sentenciaSql = "                        
                            , @estado = '$this->estado'
                            , @id_usuario_modificacion = $_SESSION[id_usuario]
                        ";        
        $this->conn->preparar($sentenciaSql);
        $this->conn->ejecutar();
    }
    
     public function eliminar(){
        $sentenciaSql = "
                            
                        ";        
        $this->conn->preparar($sentenciaSql);
        $this->conn->ejecutar();
    }
    
    public function consultar(){
        
        $condicion = $this->obtenerCondicion();
        $sentenciaSql = "					
                        ";
        $this->conn->preparar($sentenciaSql);
        $this->conn->ejecutar();
        return true;
    }
    
    
    private function obtenerCondicion(){
        $whereAnd = " WHERE ";
        $condicion = "";

        if($this->idAccion !=''){
            $condicion=$whereAnd.$condicion." municipio.idMunicipio  = $this->idMunicipio";
            $whereAnd = ' AND ';
            
        }
       if($this->estado!=''){
            if ($whereAnd == ' AND '){
                $condicion=$condicion.$whereAnd." municipio.estado = '$this->estado'";
             $whereAnd = ' AND ';
            }
            else{
                $condicion=$whereAnd.$condicion." municipio.estado = '$this->estado'";
                $whereAnd = ' AND ';
            }
       }
        return $condicion;
           
    }
    
    public function __destruct() {
        unset($this->idMunicipio);
        unset($this->idDepartamento);
        unset($this->codigo);
        unset($this->descripcion);
        unset($this->estado);
        unset($this->fechaCreacion);
        unset($this->fechaModificacion);
        unset($this->idUsuarioCreacion);
        unset($this->idUsuarioModificacion);
        unset($this->conn); 
    }
}
?>
