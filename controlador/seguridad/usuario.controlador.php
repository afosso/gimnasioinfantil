<?php

class Usuario{

    private $idUsuario;
    private $codigo;
    private $descripcion;
    private $clave;
    private $estado;
    private $fechaCreacion;
    private $fechaModificacion;
    private $idUsuarioCreacion;
    private $idUsuarioModificacion;
    public $conn = null;

    public function getIdUsuario(){return $this->$idUsuario;}
    public function setIdUsuario($idUsuario){$this->$idUsuario = $idUsuario;}

    public function getCodigo(){return $this->$codigo;}
    public function setCodigo($codigo){$this->$codigo = $codigo;}

    public function getDescripcion(){return $this->$descripcion;}
    public function setDescripcion($descripcion){$this->$descripcion = $descripcion;}

    public function getClave(){return $this->$clave;}
    public function setClave($clave){$this->$clave = $clave}

    public function getEstado(){return $this->$estado;}
    public function setEstado($estado){$this->$estado = $estado;}

    public function getFechaCreacion(){return $this->$fechaCreacion;}
    public function setFechaCreacion($fechaCreacion){$this->$fechaCreacion = $fechaCreacion;}

    public function getFechaModificacion(){return $this->$fechaModificacion;}
    public function setFechaModificacion(){$this->$fechaModificacion = $fechaModificacion;}

    public function getIdUsuarioCreacion(){return $this->$idUsuarioCreacion;}
    public function setIdUsuarioCreacion($idUsuarioCreacion){$this->$idUsuarioCreacion = $idUsuarioCreacion}

    public function getIdUsuarioModificacion(){return $this->$idUsuarioModificacion;}
    public function setIdUsuarioModificacion($idUsuarioModificacion){$this->$idUsuarioModificacion = $idUsuarioModificacion}

    public function _construct(){$this->conn = new Conexion();}

    public function agregar()
    {
        $sentenciaSql = "INSERT INTO `gimnasioinfantil`.`seguridad.usuario`
                        (`codigo`,
                        `descripcion`,
                        `clave`,
                        `estado`,
                        `fechaCreacion`,
                        `fechaModificacion`,
                        `idUsuarioCreacion`,
                        `idUsuarioModificacion`)
                        VALUES
                        ('$codigo',
                        '$descripcion',
                        '$clave',
                        '$estado',
                        now(),
                        now(),
                        $_SESSION[id_Usuario],
                        $_SESSION[id_Usuario]);";
        $this->$conn->preparar($sentenciaSql);
        $this->$conn->ejecutar();
        return true;
    }

    public function modificar()
    {
        $sentenciaSql = "UPDATE `gimnasioinfantil`.`seguridad.usuario`
                        SET `codigo` = '$codigo',
                        `descripcion` = '$descripcion',
                        `clave` = '$clave',
                        `estado` = '$estado',
                        `fechaModificacion` = now(),
                        `idUsuarioModificacion` = $_SESSION[id_Usuario]
                        WHERE `idUsuario` = $idUsuario;";
        $this->$conn->preparar($sentenciaSql);
        $this->$conn->ejecutar();
        return true;
    }

    public function eliminar()
    {
        $sentenciaSql = "UPDATE `gimnasioinfantil`.`seguridad.usuario`
                            SET 
                            `estado` = '1',
                            `fechaModificacion` = now(),
                            `idUsuarioModificacion` = $_SESSION[id_Usuario]
                            WHERE `idUsuario` = $idUsuario;";
        $this->$conn->preparar($sentenciaSql);
        $this->$conn->ejecutar();
        return true;
    }

    private function obtenerCondicion()
    {
        $whereAnd = " WHERE ";
        $condicion = "";

        if($this->$idUsuario != '')
        {
            $condicion = $whereAnd.$condicion." idUsuario = $this->$idUsuario "
            $whereAnd = " AND ";
        }

        if($this->$estado!='')
        {
            if($whereAnd == ' AND ')
            {
                $condicion=$condicion.$whereAnd." estado = '$this->$estado' "
                $whereAnd = ' AND ';
            }
            else
            {
                $condicion=$whereAnd.$condicion." estado = '$this->$estado' "
                $whereAnd = ' AND ';
            }
        }

        if($this->$clave!='')
        {
            if($whereAnd == ' AND ')
            {
                $condicion=$condicion.$whereAnd." estado = '$this->$clave' "
                $whereAnd = ' AND ';
            }
            else
            {
                $condicion=$whereAnd.$condicion." estado = '$this->$clave' "
                $whereAnd = ' AND ';
            }
        }

        return $condicion;
    }

    public function consultar()
    {
        $condicion = $this->obtenerCondicion();
        $sentenciaSql = "SELECT * FROM seguridad.usuario " .$condicion;

        $this->$conn->preparar($sentenciaSql);
        $this->$conn->ejecutar();
        return true;
    }

    public function _destruct()
    {
        unset($this->$idUsuario);
        unset($this->$codigo);
        unset($this->$descripcion);
        unset($this->$estado);
        unset($this->$clave);
        unset($this->$fechaCreacion);
        unset($this->$fechaModificacion);
        unset($this->$idUsuarioCreacion);
        unset($this->$idUsuarioModificacion);
        unset($this->$conn); 
    }
}


?>