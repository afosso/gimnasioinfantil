/*
SQLyog Trial v12.4.3 (64 bit)
MySQL - 10.1.30-MariaDB : Database - gimnasioinfantil
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`gimnasioinfantil` /*!40100 DEFAULT CHARACTER SET latin1 COLLATE latin1_spanish_ci */;

USE `gimnasioinfantil`;

/*Table structure for table `archivo.fotoactividad` */

DROP TABLE IF EXISTS `archivo.fotoactividad`;

CREATE TABLE `archivo.fotoactividad` (
  `idFotoActividad` bigint(20) NOT NULL AUTO_INCREMENT,
  `idActividad` bigint(20) NOT NULL,
  `foto` varbinary(0) NOT NULL,
  `descripcion` varchar(150) COLLATE latin1_spanish_ci NOT NULL,
  `fechaCreacion` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `fechaModificacion` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `idUsuarioCreacion` int(11) NOT NULL,
  `idUsuarioModificacion` int(11) NOT NULL,
  PRIMARY KEY (`idFotoActividad`),
  KEY `fk_idActividadFoto` (`idActividad`),
  CONSTRAINT `fk_idActividadFoto` FOREIGN KEY (`idActividad`) REFERENCES `general.actividad` (`idActividad`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_spanish_ci;

/*Data for the table `archivo.fotoactividad` */

/*Table structure for table `empresa.empresa` */

DROP TABLE IF EXISTS `empresa.empresa`;

CREATE TABLE `empresa.empresa` (
  `idEmpresa` int(11) NOT NULL AUTO_INCREMENT,
  `nit` varchar(50) COLLATE latin1_spanish_ci NOT NULL,
  `digitoVerificacion` varchar(50) COLLATE latin1_spanish_ci NOT NULL,
  `razonSocial` varchar(200) COLLATE latin1_spanish_ci NOT NULL,
  `idMunicipio` bigint(20) NOT NULL,
  `direccion` varchar(200) COLLATE latin1_spanish_ci NOT NULL,
  `telefono` varchar(50) COLLATE latin1_spanish_ci NOT NULL,
  `celular` varchar(50) COLLATE latin1_spanish_ci NOT NULL,
  `correoElectronico` varchar(200) COLLATE latin1_spanish_ci NOT NULL,
  `representanteLegal` varchar(200) COLLATE latin1_spanish_ci NOT NULL,
  `cantidadFotosActividad` int(11) NOT NULL,
  `tamanioMaximo` int(11) NOT NULL,
  `fechaCreacion` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `fechaModificacion` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `idUsuarioCreacion` int(11) NOT NULL,
  `idUsuarioModificacion` int(11) NOT NULL,
  PRIMARY KEY (`idEmpresa`),
  KEY `fk_idMunicipioEmpresa` (`idMunicipio`),
  CONSTRAINT `fk_idMunicipioEmpresa` FOREIGN KEY (`idMunicipio`) REFERENCES `ubicacion.municipio` (`idMunicipio`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_spanish_ci;

/*Data for the table `empresa.empresa` */

/*Table structure for table `general.actividad` */

DROP TABLE IF EXISTS `general.actividad`;

CREATE TABLE `general.actividad` (
  `idActividad` bigint(20) NOT NULL AUTO_INCREMENT,
  `idGrado` int(11) NOT NULL,
  `descripcion` varchar(250) COLLATE latin1_spanish_ci NOT NULL,
  `tema` varchar(200) COLLATE latin1_spanish_ci NOT NULL,
  `fecha` date NOT NULL,
  `objetivo` varchar(200) COLLATE latin1_spanish_ci NOT NULL,
  `materiales` varchar(1000) COLLATE latin1_spanish_ci NOT NULL,
  `metodologias` varchar(1000) COLLATE latin1_spanish_ci NOT NULL,
  `experiencia` varchar(250) COLLATE latin1_spanish_ci NOT NULL,
  `fechaCreacion` datetime NOT NULL,
  `fechaModificacion` datetime NOT NULL,
  `idUsuarioCreacion` int(11) NOT NULL,
  `idUsuarioModificacion` int(11) NOT NULL,
  PRIMARY KEY (`idActividad`),
  KEY `idGrago` (`idGrado`),
  CONSTRAINT `idGrago` FOREIGN KEY (`idGrado`) REFERENCES `general.grado` (`idGrado`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_spanish_ci;

/*Data for the table `general.actividad` */

/*Table structure for table `general.grado` */

DROP TABLE IF EXISTS `general.grado`;

CREATE TABLE `general.grado` (
  `idGrado` int(11) NOT NULL AUTO_INCREMENT,
  `codigo` varchar(50) COLLATE latin1_spanish_ci NOT NULL,
  `descripcion` varchar(150) COLLATE latin1_spanish_ci NOT NULL,
  `estado` tinyint(1) NOT NULL DEFAULT '1',
  `fechaCreacion` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `fechaModificacion` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `idUsuarioCreacion` int(11) NOT NULL,
  `idUsuarioModificacion` int(11) NOT NULL,
  PRIMARY KEY (`idGrado`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_spanish_ci;

/*Data for the table `general.grado` */

/*Table structure for table `seguridad.rol` */

DROP TABLE IF EXISTS `seguridad.rol`;

CREATE TABLE `seguridad.rol` (
  `idRol` int(11) NOT NULL AUTO_INCREMENT,
  `codigo` varchar(50) COLLATE latin1_spanish_ci NOT NULL,
  `descripcion` varchar(150) COLLATE latin1_spanish_ci NOT NULL,
  `estado` tinyint(1) NOT NULL DEFAULT '1',
  `fechaCreacion` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `fechaModificacion` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `idUsuarioCreacion` int(11) NOT NULL,
  `idUsuarioModificacion` int(11) NOT NULL,
  PRIMARY KEY (`idRol`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_spanish_ci;

/*Data for the table `seguridad.rol` */

/*Table structure for table `seguridad.rolusuario` */

DROP TABLE IF EXISTS `seguridad.rolusuario`;

CREATE TABLE `seguridad.rolusuario` (
  `idRolUsuario` bigint(20) NOT NULL AUTO_INCREMENT,
  `idRol` int(11) NOT NULL,
  `idUsuario` int(11) NOT NULL,
  `fechaCreacion` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `fechaModificacion` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `idUsuarioCreacion` int(11) NOT NULL,
  `idUsuarioModificacion` int(11) NOT NULL,
  PRIMARY KEY (`idRolUsuario`),
  KEY `fk_idUsuario` (`idUsuario`),
  KEY `fk_idRol` (`idRol`),
  CONSTRAINT `fk_idRol` FOREIGN KEY (`idRol`) REFERENCES `seguridad.rol` (`idRol`),
  CONSTRAINT `fk_idUsuario` FOREIGN KEY (`idUsuario`) REFERENCES `seguridad.usuario` (`idUsuario`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_spanish_ci;

/*Data for the table `seguridad.rolusuario` */

/*Table structure for table `seguridad.usuario` */

DROP TABLE IF EXISTS `seguridad.usuario`;

CREATE TABLE `seguridad.usuario` (
  `idUsuario` int(11) NOT NULL AUTO_INCREMENT,
  `codigo` varchar(50) COLLATE latin1_spanish_ci NOT NULL,
  `descripcion` varchar(150) COLLATE latin1_spanish_ci NOT NULL,
  `clave` varchar(200) COLLATE latin1_spanish_ci NOT NULL,
  `estado` tinyint(1) NOT NULL DEFAULT '1',
  `fechaCreacion` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `fechaModificacion` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `idUsuarioCreacion` int(11) NOT NULL,
  `idUsuarioModificacion` int(11) NOT NULL,
  PRIMARY KEY (`idUsuario`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_spanish_ci;

/*Data for the table `seguridad.usuario` */

/*Table structure for table `tercero.tercero` */

DROP TABLE IF EXISTS `tercero.tercero`;

CREATE TABLE `tercero.tercero` (
  `idTercero` bigint(20) NOT NULL AUTO_INCREMENT,
  `idMunicipio` bigint(20) NOT NULL,
  `numeroIdentificacion` varchar(50) COLLATE latin1_spanish_ci NOT NULL,
  `primerNombre` varchar(150) COLLATE latin1_spanish_ci NOT NULL,
  `segundoNombre` varchar(150) COLLATE latin1_spanish_ci DEFAULT NULL,
  `primerApellido` varchar(150) COLLATE latin1_spanish_ci NOT NULL,
  `segundoApellido` varchar(150) COLLATE latin1_spanish_ci DEFAULT NULL,
  `telefono` varchar(50) COLLATE latin1_spanish_ci DEFAULT NULL,
  `celular` varchar(50) COLLATE latin1_spanish_ci NOT NULL,
  `direccion` varchar(150) COLLATE latin1_spanish_ci NOT NULL,
  `correoElectronico` varchar(150) COLLATE latin1_spanish_ci NOT NULL,
  `foto` binary(1) DEFAULT NULL,
  `esTercero` tinyint(1) NOT NULL DEFAULT '0',
  `esPadreFamilia` tinyint(1) NOT NULL DEFAULT '0',
  `esEmpleado` tinyint(1) NOT NULL DEFAULT '0',
  `esProfesor` tinyint(1) NOT NULL DEFAULT '0',
  `estado` tinyint(1) NOT NULL DEFAULT '1',
  `fechaCreacion` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `fechaModificacion` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `idUsuarioCreacion` int(11) NOT NULL,
  `idUsuarioModificacion` int(11) NOT NULL,
  PRIMARY KEY (`idTercero`),
  KEY `fk_idMunicipio` (`idMunicipio`),
  CONSTRAINT `fk_idMunicipio` FOREIGN KEY (`idMunicipio`) REFERENCES `ubicacion.municipio` (`idMunicipio`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_spanish_ci;

/*Data for the table `tercero.tercero` */

/*Table structure for table `ubicacion.departamento` */

DROP TABLE IF EXISTS `ubicacion.departamento`;

CREATE TABLE `ubicacion.departamento` (
  `idDepartamento` bigint(20) NOT NULL AUTO_INCREMENT,
  `idPais` int(11) NOT NULL,
  `codigo` varchar(50) COLLATE latin1_spanish_ci NOT NULL,
  `descripcion` varchar(150) COLLATE latin1_spanish_ci NOT NULL,
  `estado` tinyint(1) NOT NULL DEFAULT '1',
  `fechaCreacion` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `fechaModificacion` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `idUsuarioCreacion` int(11) NOT NULL,
  `idUsuarioModificacion` int(11) NOT NULL,
  PRIMARY KEY (`idDepartamento`),
  KEY `fk_idPais` (`idPais`),
  CONSTRAINT `fk_idPais` FOREIGN KEY (`idPais`) REFERENCES `ubicacion.pais` (`idPais`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_spanish_ci;

/*Data for the table `ubicacion.departamento` */

/*Table structure for table `ubicacion.municipio` */

DROP TABLE IF EXISTS `ubicacion.municipio`;

CREATE TABLE `ubicacion.municipio` (
  `idMunicipio` bigint(20) NOT NULL AUTO_INCREMENT,
  `idDepartamento` bigint(20) NOT NULL,
  `codigo` varchar(50) COLLATE latin1_spanish_ci NOT NULL,
  `descripcion` varchar(150) COLLATE latin1_spanish_ci NOT NULL,
  `estado` tinyint(1) NOT NULL DEFAULT '1',
  `fechaCreacion` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `fechaModificacion` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `idUsuarioCreacion` int(11) NOT NULL,
  `idUsuarioModificacion` int(11) NOT NULL,
  PRIMARY KEY (`idMunicipio`),
  KEY `fk_idDepartamento` (`idDepartamento`),
  CONSTRAINT `fk_idDepartamento` FOREIGN KEY (`idDepartamento`) REFERENCES `ubicacion.departamento` (`idDepartamento`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_spanish_ci;

/*Data for the table `ubicacion.municipio` */

/*Table structure for table `ubicacion.pais` */

DROP TABLE IF EXISTS `ubicacion.pais`;

CREATE TABLE `ubicacion.pais` (
  `idPais` int(11) NOT NULL AUTO_INCREMENT,
  `codigo` varchar(50) COLLATE latin1_spanish_ci NOT NULL,
  `descripcion` varchar(150) COLLATE latin1_spanish_ci NOT NULL,
  `estado` tinyint(1) NOT NULL DEFAULT '1',
  `fechaCreacion` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `fechaModificacion` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `idUsuarioCreacion` int(11) NOT NULL,
  `idUsuarioModificacion` int(11) NOT NULL,
  PRIMARY KEY (`idPais`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_spanish_ci;

/*Data for the table `ubicacion.pais` */

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
