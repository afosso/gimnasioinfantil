function Login(usuario, contrasenia)
{
    var parametros = 
    {
        "accion" : "CONSULTAR_INICIO_SESION",
        "txtCodigo" : usuario,
        "txtClave" : contrasenia
    }

    $.ajax({
        data: parametros,
        url: "../../modelo/seguridad/usuario.modelo.php",
        type: "post",
        success: function(response){
            location.href = '../../index.html';
        },
        error: function()
        {
            alert("ERROR");
        }
    });
}